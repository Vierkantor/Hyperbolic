from hyperbolic.coordinate import Coordinate

"""The number of tiles that fit in a chunk."""
# COMPAT: keep this in sync with position.rs
_chunk_size = 16

class Chunk:
	"""Represents a contiguous piece of the world.

	In contrast to the situation between chunks,
	within a chunk everything is on a nice Euclidean plane.
	"""

	def __init__(self, coordinate: Coordinate, tiles):
		self.coordinate = coordinate
		self.tiles = tiles

"""All chunks are currently stored in this dictionary.

To get a chunk, you probably want to use get_chunk.
"""
_chunk_map = {}

def _generate_chunk(coordinate: Coordinate):
	"""Create a new chunk at the given coordinate.
	
	The contents of the chunk will be initialized with some generic junk.
	"""
	return Chunk(coordinate, {(x, y, z): None
		for x in range(0, _chunk_size)
		for y in range(0, _chunk_size)
		for z in range(0, _chunk_size)
	})

def get_chunk(coordinate: Coordinate):
	"""Get the chunk at the given coordinate.
	
	If the chunk didn't exist yet, it will be generated.
	"""
	try:
		chunk = _chunk_map[coordinate]
	except KeyError:
		chunk = _generate_chunk(coordinate)
		_chunk_map[coordinate] = chunk
	return chunk
