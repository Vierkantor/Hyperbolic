import functools
import random

from dbecs.entity import Entity
from hyperbolic.graphics import rgb_color, DrawQueue, position
from hyperbolic.input import keyboard_table
from hyperbolic.position import ChunkPosition, Positioned
from hyperbolic.world import World

def initialize(args):
	"""Start the simulation with given arguments."""
	global world
	world = World()

def update(dt):
	"""Update the simulation, with a given time step."""
	pass

@functools.lru_cache(maxsize=256)
def get_coordinate(player, dx, dy):
	player_pos = Positioned(world.player)
	tile_pos = ChunkPosition(
			player_pos.coordinate,
			player_pos.local_x, player_pos.local_y, player_pos.local_z,
			player_pos.rotation,
	)
	tile_pos.go_relative(dx, dy, 0)
	return tile_pos._coordinate()

@functools.lru_cache()
def get_color(coordinate):
	random.seed(coordinate)
	return rgb_color(random.random(), random.random(), random.random(), 1)

def draw(width, height):
	"""Draw the whole game, with given width and height of the rendered area."""
	queue = DrawQueue()
	
	for dx in range(-19, 20):
		for dy in range(-19, 20):
			coordinate = get_coordinate(world.player, dx, dy)
			queue.rectangle(
					get_color(coordinate),
					position(500 + 10 * dy, 500 - 10 * dx, 10, 10))

	queue.ellipse(rgb_color(1, 0, 0, 1), position(500, 500, 10, 10))
	
	return queue

def input(pressed, key):
	"""Called when a key is pressed or released.
	
	pressed is True when the key is pressed, False when released.
	key is an int representing the SDL key code, as in input.keyboard_table.
	"""

	if pressed:
		player_pos = Positioned(world.player)
		if key == keyboard_table.up:
			player_pos.go_forward()
			get_coordinate.cache_clear()
		if key == keyboard_table.left:
			player_pos.turn_left()
			get_coordinate.cache_clear()
		if key == keyboard_table.right:
			player_pos.turn_right()
			get_coordinate.cache_clear()
		if key == keyboard_table.down:
			player_pos.move(-1, 0, 0)
			get_coordinate.cache_clear()
