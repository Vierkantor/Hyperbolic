from typing import Tuple

from hyperbolic.coordinate.kbword import Word, Coordinate
from hyperbolic._rust import _normalize, neighbors, neighbor_index

def _shortlex_min(*words: Word) -> bool:
	"""Give the minimum of the words according to the shortlex order.
	
	An empty argument list raises a ValueError.
	"""
	try:
		minimum = words[0]
	except IndexError:
		raise ValueError("minimum of zero values")
	for word in words:
		if len(word) < len(minimum):
			minimum = word
		elif len(word) == len(minimum) and word < minimum:
			minimum = word
	return minimum


"""The coordinate that represents the origin."""
origin : Coordinate = ''

def neighbors_from(tile: Coordinate, start: Coordinate) -> Tuple[Coordinate, Coordinate, Coordinate, Coordinate]:
	"""Give the four neighbors of a tile, starting from the given coordinate.
	If the start is not a neighbor, raises a ValueError.
	
	The neighbors are listed in clockwise order.
	(i.e. from neighbors[0] to neighbors[1] you turn 90 degrees to the right.)
	"""
	neighborhood = neighbors(tile)
	i = neighbor_index(tile, start)
	return neighborhood[i:] + neighborhood[:i]
