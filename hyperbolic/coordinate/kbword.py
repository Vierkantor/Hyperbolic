from typing import Tuple

# Type definitions from Rust
Coordinate = Word = str

def _shortlex_min(*words: Word) -> bool:
	"""Give the minimum of the words according to the shortlex order.
	
	An empty argument list raises a ValueError.
	"""
	try:
		minimum = words[0]
	except IndexError:
		raise ValueError("minimum of zero values")
	for word in words:
		if len(word) < len(minimum):
			minimum = word
		elif len(word) == len(minimum) and word < minimum:
			minimum = word
	return minimum

def _shorten_step(word : Word) -> Word:
	"""The Knuth-Bendix algorithm gives a rewrite system consisting of
	these steps applied repeatedly.
	
	This function is exposed so it can be tested.
	"""
	return word \
		.replace("yxy", "xxx") \
		.replace("xxxx", "") \
		.replace("yyyy", "xyx") \
		.replace("xxxyyy", "yxxyx") \
		.replace("xyxxyx", "yyy") \
		.replace("yyyxxx", "xyxxy") \
		.replace("xxxyyxxx", "yxxyxxy") \
		.replace("xyxxyyxxyx", "yyyxxyyy") \
		.replace("xyxxyyxxyyxxyx", "yyyxxyyyxxyyy") \
		.replace("yyyxxyyyxxyyyxxyyy", "xyxxyyxxyyxxyyxxyx") \
		.replace("yxxyxxyyxxyyxxyyxxyx", "xxxyyxxyyyxxyyyxxyyy") \
		.replace("yyyxxyyyxxyyyxxyyxxx", "xyxxyyxxyyxxyyxxyxxy")

def _shorten(word : Word) -> Word:
	"""Internal function for uniquifying a word's representation.
	
	You shouldn't use this for making coordinates as it does not consider rotations
	of the same word to be equal.
	"""
	shorter = _shorten_step(word)
	while shorter != word:
		word = shorter
		shorter = _shorten_step(word)
	return shorter

def _normalize(word : Word) -> Coordinate:
	"""Internal function to convert a word to a coordinate.
	
	Takes the minimum of all rotations.
	"""
	return _shortlex_min(
			_shorten(word + ''),
			_shorten(word + 'x'),
			_shorten(word + 'xx'),
			_shorten(word + 'xxx'),
	)

"""The words for the squares of distance 1 to the origin.

Note that these are words, not coordinates, as we preserve the orientation.
"""
# COMPAT: should be the same as in coordinate.rs
_north : Word = 'yxxx'
_east : Word = 'xyxx'
_south : Word = 'xxyx'
_west : Word = 'xxxy'

def neighbors(tile: Coordinate) -> Tuple[Coordinate, Coordinate, Coordinate, Coordinate]:
	"""Give the four neighbors of a tile with given coordinates.
	
	The neighbors are listed in clockwise order.
	(i.e. from neighbors[0] to neighbors[1] you turn 90 degrees to the right.)
	"""
	return (
		_normalize(tile + _north),
		_normalize(tile + _east),
		_normalize(tile + _south),
		_normalize(tile + _west),
	)

def neighbor_index(tile: Coordinate, neighbor: Coordinate) -> int:
	"""Give the index of the neighbor in the list of neighbors.
	If not found, raises a ValueError.
	
	The neighbors are listed in clockwise order.
	(i.e. from neighbors[0] to neighbors[1] you turn 90 degrees to the right.)
	"""
	neighborhood = neighbors(tile)
	for i, maybe_neighbor in enumerate(neighborhood):
		if maybe_neighbor == neighbor:
			return i
	else:
		raise ValueError(f"tile '{neighbor}' is not a neighbor of '{tile}'")

