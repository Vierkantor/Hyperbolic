# Re-export some definitions from Rust into a logical location.

from . import _rust

class KeyboardTable:
	"""An object with attributes for each key, giving its code.

	You probably want to treat this as a singleton,
	using the provided instance keyboard_table of this module.
	"""
	
	def __init__(self):
		for key, code in _rust.make_keyboard_table().items():
			setattr(self, key, code)

keyboard_table = KeyboardTable()
