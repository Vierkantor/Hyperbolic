import math
import sqlalchemy

from dbecs.entity import Value, Component, add_value_type
from hyperbolic.chunk import _chunk_size
from hyperbolic.coordinate import Coordinate, neighbors, neighbor_index, origin
from hyperbolic._rust import ChunkPosition

# We'll store the position in the database in a custom way.
add_value_type(ChunkPosition, None)

"""Represent rotations as an int mod 4."""
Rotation = int
"""Rotations in clockwise order."""
rotations \
	= north, east, south, west \
	= minus_y, plus_x, plus_y, minus_x \
	= 0, 1, 2, 3
"""The displacements corresponding with these rotations,
in local x and y coordinates.
"""
# COMPAT: these should match the order of a chunk's neighbors
displacements = (0, -1), (1, 0), (0, 1), (-1, 0)

class Positioned(Component):
	"""Represents a thing with location and rotation.
	
	To avoid excessive allocation / deallocation,
	a position is updated in-place where feasible.
	"""
	pos = Value(ChunkPosition)

	@property
	def coordinate(self):
		return self.pos._coordinate()
	@coordinate.setter
	def coordinate(self, value):
		self.pos._set_coordinate(value)

	@property
	def local_x(self):
		return self.pos._local_x()
	@local_x.setter
	def local_x(self, value):
		self.pos._set_local_x(value)

	@property
	def local_y(self):
		return self.pos._local_y()
	@local_y.setter
	def local_y(self, value):
		self.pos._set_local_y(value)

	@property
	def local_z(self):
		return self.pos._local_z()
	@local_z.setter
	def local_z(self, value):
		self.pos._set_local_z(value)

	@property
	def rotation(self):
		return self.pos._rotation()
	@rotation.setter
	def rotation(self, value):
		self.pos._set_rotation(value)

	@classmethod
	def register(cls, entity, coordinate=origin, local_x=0, local_y=0, local_z=0, rotation=0):
		self = cls(entity)
		self.pos = ChunkPosition(coordinate, local_x, local_y, local_z, rotation)

	@classmethod
	def generate_table(cls, metadata):
		rows = [
				sqlalchemy.Column('_entity', sqlalchemy.Integer),
				sqlalchemy.Column('coordinate', sqlalchemy.String),
				sqlalchemy.Column('local_x', sqlalchemy.Integer),
				sqlalchemy.Column('local_y', sqlalchemy.Integer),
				sqlalchemy.Column('local_z', sqlalchemy.Integer),
				sqlalchemy.Column('rotation', sqlalchemy.Integer),
		]
		return sqlalchemy.Table(cls.__name__, metadata, *rows)

	@classmethod
	def save(cls, attrs):
		pos = attrs['pos']
		del attrs['pos']
		return {**attrs,
			'coordinate': pos._coordinate(),
			'local_x': pos._local_x(),
			'local_y': pos._local_y(),
			'local_z': pos._local_z(),
			'rotation': pos._rotation(),
		}

	@classmethod
	def load(cls, entity, attr_list):
		"""Apply this component from a list of attributes.

		This method is called after Component.register.
		"""
		attrs = dict(attr_list)
		entity.components[cls]["pos"] = ChunkPosition(
			attrs['coordinate'],
			attrs['local_x'],
			attrs['local_y'],
			attrs['local_z'],
			attrs['rotation'],
		)
		del attrs['coordinate']
		del attrs['local_x']
		del attrs['local_y']
		del attrs['local_z']
		del attrs['rotation']
		super().load(entity, list(attrs.items()))

	def _relative_position(self, coordinate):
		"""Get the local_x, local_y, local_z and rotation
		relative to a neighbor of the current chunk."""

		# First move to the other chunk, ignoring rotations.
		here_to_there = neighbor_index(self.coordinate, coordinate)
		dx, dy = displacements[here_to_there]
		x = self.local_x - _chunk_size * dx
		y = self.local_y - _chunk_size * dy

		# Then rotate to match the angle between the chunks.
		there_to_here = neighbor_index(coordinate, self.coordinate)
		rotation = (there_to_here - here_to_there + 2) % 4
		for _ in range(0, rotation):
			x, y = _chunk_size - y - 1, x

		return x, y, self.local_z, self.rotation + rotation

	def _cross_boundary(self, new_coordinate):
		"""Cross a chunk boundary to a neighbor of this chunk,
		also updating our local coordinates.

		The given chunk must be a neighbor of this chunk.
		(Otherwise, which homotopy class of paths did we use to get there?)
		"""
		self.local_x, self.local_y, self.local_z, self.rotation \
			= self._relative_position(new_coordinate)
		self.coordinate = new_coordinate

	def go_forward(self, distance=1):
		"""Update the location to some distance ahead.
		This is relative to the rotation of this position.
		
		See documentation for go_orthogonal for limitations on this function.
		"""
		self.go_orthogonal(self.rotation, distance)
	
	def go_orthogonal(self, rotation, distance=1):
		"""Update the location to some distance in the given chunk-local rotation.

		If the distance is not in range(0, _chunk_size + 1),
		a ValueError is raised.

		Note that we can't easily implement moving in two orthogonal directions,
		as in general going some distance along the x axis, followed by some
		distance along the y axis, does not give the same result as first
		going along the y axis and then the x axis.
		For a method that does support this (though much slower),
		see the move function.
		"""
		self.pos.go_orthogonal(rotation, distance)

	def turn_left(self):
		"""Update the position to face left relative to this orientation.
		
		(This is the same as turning counterclockwise.)
		"""
		self.rotation = (self.rotation - 1) % 4

	def turn_right(self):
		"""Update the position to face right relative to this orientation.
		
		(This is the same as turning clockwise.)
		"""
		self.rotation = (self.rotation + 1) % 4

	def move(self, forward, right, up):
		"""Update the location to the given displacement,
		as distance relative to this entity's rotation.

		The path we travel is a straight line,
		so it may need to be updated when reaching the chunk border.

		This takes into account paths that leave the chunk,
		and paths that pass through the corner of a chunk.
		(If the path is steeper in the x direction, we cross the x border first,
		and similarly for the y direction.)
		When we cross a corner exactly diagonally,
		the borders we cross are in unspecified order.
		"""

		self.pos.go_relative(forward, right, up)
