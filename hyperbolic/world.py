from dbecs.entity import Entity
from hyperbolic.coordinate import origin
from hyperbolic.position import Positioned, north

class World:
	"""Represents the current status of the simulation."""
	def __init__(self):
		self.player = Entity()
		self.player.add_component(Positioned, origin, 0, 0, 0, north)
