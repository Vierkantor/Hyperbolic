import sys

from . import _rust

from hyperbolic import running

running.initialize(sys.argv)
_rust.main(running.update, running.draw, running.input)
