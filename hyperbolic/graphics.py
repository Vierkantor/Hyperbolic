# Re-export some definitions from Rust into a logical location.

from . import _rust

def rgb_color(r, g, b, a=1.0):
	"""Make a Color object with the given RGB[A] components.
	
	All components should be numbers between 0.0 and 1.0.
	"""
	return _rust.Color(float(r), float(g), float(b), float(a))

# On this class, you can call methods corresponding to the shapes to draw.
DrawQueue = _rust.DrawQueue

def position(x, y, w, h):
	"""Represent a position on the screen with x and y coordinates,
	and a width and height.
	
	All arguments should be numbers castable to float.
	"""
	return _rust.Position(float(x), float(y), float(w), float(h))
