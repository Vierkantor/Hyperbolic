PYTHON ?= python3
PYTEST ?= pytest

.PHONY: optimized testable
optimized:
	./setup.py build_rust --inplace --release
testable:
	./setup.py build_rust --inplace --debug

.PHONY: rust rust-test rust-bench
rust:
	$(MAKE) -C extensions all
rust-test:
	$(MAKE) -C extensions test
rust-bench:
	$(MAKE) -C extensions bench

.PHONY: run test bench
run: optimized
	${PYTHON} -m hyperbolic
test: testable
	${PYTEST} test
bench: optimized rust-bench
	${PYTEST} bench
