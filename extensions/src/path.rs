#[cfg(test)]
use quickcheck::Arbitrary;
#[cfg(test)]
use quickcheck::Gen;

// A step in an absolute direction.
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum AbsStep {
    North,
    East,
    South,
    West,
}

// A step in a relative direction.
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum RelStep {
    Forward,
    Right,
    Backward,
    Left,
}

#[cfg(test)]
impl Arbitrary for AbsStep {
    fn arbitrary<G: Gen>(g: &mut G) -> AbsStep {
        match g.gen_range(0, 4) {
            0 => AbsStep::North,
            1 => AbsStep::East,
            2 => AbsStep::South,
            3 => AbsStep::West,
            _ => panic!("Generated value out of range for enum"),
        }
    }
}

#[cfg(test)]
impl Arbitrary for RelStep {
    fn arbitrary<G: Gen>(g: &mut G) -> RelStep {
        match g.gen_range(0, 4) {
            0 => RelStep::Forward,
            1 => RelStep::Right,
            2 => RelStep::Backward,
            3 => RelStep::Left,
            _ => panic!("Generated value out of range for enum"),
        }
    }
}

// A path between two places represented as a list of steps.
// Given a starting point, this uniquely determines an ending point.
pub type AbsPath = Vec<AbsStep>;
// Given a starting point and orientation,
// this uniquely determines an ending point.
pub type RelPath = Vec<RelStep>;

// Get the offset between the last direction and the next one.
pub fn abs_difference(last_dir: AbsStep, next_dir: AbsStep) -> RelStep {
    match last_dir {
        AbsStep::North => match next_dir {
            AbsStep::North => RelStep::Forward,
            AbsStep::East => RelStep::Right,
            AbsStep::South => RelStep::Backward,
            AbsStep::West => RelStep::Left,
        },
        AbsStep::East => match next_dir {
            AbsStep::North => RelStep::Left,
            AbsStep::East => RelStep::Forward,
            AbsStep::South => RelStep::Right,
            AbsStep::West => RelStep::Backward,
        },
        AbsStep::South => match next_dir {
            AbsStep::North => RelStep::Backward,
            AbsStep::East => RelStep::Left,
            AbsStep::South => RelStep::Forward,
            AbsStep::West => RelStep::Right,
        },
        AbsStep::West => match next_dir {
            AbsStep::North => RelStep::Right,
            AbsStep::East => RelStep::Backward,
            AbsStep::South => RelStep::Left,
            AbsStep::West => RelStep::Forward,
        },
    }
}
pub fn rel_sum(last_dir: AbsStep, difference: RelStep) -> AbsStep {
    match last_dir {
        AbsStep::North => match difference {
            RelStep::Forward => AbsStep::North,
            RelStep::Right => AbsStep::East,
            RelStep::Backward => AbsStep::South,
            RelStep::Left => AbsStep::West,
        },
        AbsStep::East => match difference {
            RelStep::Forward => AbsStep::East,
            RelStep::Right => AbsStep::South,
            RelStep::Backward => AbsStep::West,
            RelStep::Left => AbsStep::North,
        },
        AbsStep::South => match difference {
            RelStep::Forward => AbsStep::South,
            RelStep::Right => AbsStep::West,
            RelStep::Backward => AbsStep::North,
            RelStep::Left => AbsStep::East,
        },
        AbsStep::West => match difference {
            RelStep::Forward => AbsStep::West,
            RelStep::Right => AbsStep::North,
            RelStep::Backward => AbsStep::East,
            RelStep::Left => AbsStep::South,
        },
    }
}

// Convert paths between the two formats.
// Since an empty absolute path has no starting orientation,
// we can't completely convert it.
pub fn abs_to_rel(abs_path: &AbsPath) -> Option<(AbsStep, RelPath)> {
    if abs_path.len() == 0 {
        None
    } else {
        let mut rel_path = RelPath::new();
        let mut direction: AbsStep = abs_path[0];
        for next_direction in abs_path.iter() {
            rel_path.push(abs_difference(direction, next_direction.clone()));
            direction = next_direction.clone();
        }
        Some((abs_path[0], rel_path))
    }
}
pub fn rel_to_abs(direction: AbsStep, rel_path: &RelPath) -> AbsPath {
    // Note that we don't add the first direction:
    // it is used to encode the last direction taken before the path starts.
    let mut abs_path = AbsPath::new();
    let mut cur_direction = direction;
    for difference in rel_path.iter() {
        cur_direction = rel_sum(cur_direction, difference.clone());
        abs_path.push(cur_direction);
    }
    abs_path
}
