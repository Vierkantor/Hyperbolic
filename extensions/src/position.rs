use cpython::{Python, PyErr, PyModule, PyObject, PyResult, PyString};
use cpython::{NoArgs, ToPyObject};
use cpython::exc::{ValueError};
use std::cell::{Cell, RefCell};
use std::ops::{Deref};

use coordinate::{Coordinate, neighbors, neighbor_index};

// COMPAT: keep this in sync with the one in chunk.py
const CHUNK_SIZE: i8 = 16;

const DISPLACEMENTS_X: [i8; 4] = [0, 1, 0, -1];
const DISPLACEMENTS_Y: [i8; 4] = [-1, 0, 1, 0];

const MINUS_Y: i8 = 0;
const PLUS_X: i8 = 1;
const PLUS_Y: i8 = 2;
const MINUS_X: i8 = 3;

pub fn initialize_module(py: Python, m: &PyModule) -> PyResult<()> {
    m.add(py, "ChunkPosition", py.get_type::<ChunkPosition>())?;
    Ok(())
}

py_class!(class ChunkPosition |py| {
    data coordinate: RefCell<Coordinate>;
    data local_x: Cell<i8>;
    data local_y: Cell<i8>;
    data local_z: Cell<i8>;
    data rotation: Cell<usize>;

    def __new__(_cls, coordinate: Coordinate, x: i8, y: i8, z: i8, rotation: usize)
        -> PyResult<ChunkPosition>
    {
        ChunkPosition::create_instance(py,
            RefCell::new(coordinate),
            Cell::new(x), Cell::new(y), Cell::new(z),
            Cell::new(rotation))
    }

    def _coordinate(&self) -> PyResult<PyString> {
        Ok(self.coordinate(py).borrow().to_py_object(py))
    }
    def _set_coordinate(&self, value: Coordinate) -> PyResult<PyObject> {
        let mut coord_ref = self.coordinate(py).borrow_mut();
        *coord_ref = value;
        Ok(py.None())
    }
    def _local_x(&self) -> PyResult<i8> {
        Ok(self.local_x(py).get())
    }
    def _set_local_x(&self, value: i8) -> PyResult<PyObject> {
        self.local_x(py).set(value);
        Ok(py.None())
    }
    def _local_y(&self) -> PyResult<i8> {
        Ok(self.local_y(py).get())
    }
    def _set_local_y(&self, value: i8) -> PyResult<PyObject> {
        self.local_y(py).set(value);
        Ok(py.None())
    }
    def _local_z(&self) -> PyResult<i8> {
        Ok(self.local_z(py).get())
    }
    def _set_local_z(&self, value: i8) -> PyResult<PyObject> {
        self.local_z(py).set(value);
        Ok(py.None())
    }
    def _rotation(&self) -> PyResult<usize> {
        Ok(self.rotation(py).get())
    }
    def _set_rotation(&self, value: usize) -> PyResult<PyObject> {
        self.rotation(py).set(value);
        Ok(py.None())
    }

    def go_orthogonal(&self, rotation: usize, distance: i8) -> PyResult<PyObject> {
        if distance < 0 || distance > CHUNK_SIZE {
            return Err(PyErr::new::<ValueError, NoArgs>(py, NoArgs));
        }
        let mut local_x = self.local_x(py).get();
        let mut local_y = self.local_y(py).get();
        assert!(0 <= local_x && local_x < CHUNK_SIZE);
        assert!(0 <= local_y && local_y < CHUNK_SIZE);

        // Add the displacement, not taking into account borders...
        local_x += DISPLACEMENTS_X[rotation] * distance;
        local_y += DISPLACEMENTS_Y[rotation] * distance;
        self.local_x(py).set(local_x);
        self.local_y(py).set(local_y);

        // ... and wrap around the chunk border.
        if 0 <= local_x && local_x < CHUNK_SIZE
            && 0 <= local_y && local_y < CHUNK_SIZE
        {
            // We stayed within the chunk, nothing more to do.
            return Ok(py.None());
        }
        let neighborhood = neighbors(self.coordinate(py).borrow().deref());
        let new_coordinate = neighborhood[rotation].clone();

        // Update our position into the new chunk's coordinate system.
        self.cross_boundary(py, new_coordinate, rotation);

        Ok(py.None())
    }

    def go_relative(&self, forward_: i8, right: i8, up: i8) -> PyResult<PyObject> {
        // We can safely add the vertical displacement,
        // as that is completely euclidean.
        let local_z = self.local_z(py);
        local_z.set(local_z.get() + up);

        // Nothing more to do if we can't cross a chunk boundary.
        if forward_ == 0 && right == 0 {
            return Ok(py.None());
        }

        // We can act like we're drawing a line in an Euclidean grid,
        // but we go to a new chunk when needed.
        // For drawing the line, we use Bresenham's algorithm.
        // (Of course, this will take O(|dx| + |dy|) operations,
        // so there might be room for improvement!)

        // First determine in which octant our path lies.
        // (i.e. which chunk boundaries to cross.)
        // Represent our direction as an offset from our current rotation.
        let mut forward_rotation = 0;
        let mut sideways_rotation = 1;
        let mut forward = forward_;
        let mut sideways = right;
        // We should move at least as much forward as to the side.
        // If not, swap the directions.
        if forward.abs() < right.abs() {
            sideways = forward;
            forward = right;
            forward_rotation = 1;
            sideways_rotation = 0;
        }
        // The displacements must be positive, go the other direction if not.
        if forward < 0 {
            forward *= -1;
            forward_rotation = (forward_rotation + 2) % 4;
        }
        if sideways < 0 {
            sideways *= -1;
            sideways_rotation = (sideways_rotation + 2) % 4;
        }

        // Count the required sideways displacement.
        // (If this is > 0, then we should go sideways.)
        let mut D = 2 * sideways - forward;
        // Go forward, sometimes stepping to the side.
        let rot = self.rotation(py);
        for _ in 0..forward {
            self.go_orthogonal(py, (rot.get() + forward_rotation) % 4, 1)?;
            if D > 0 {
                self.go_orthogonal(py, (rot.get() + sideways_rotation) % 4, 1)?;
                D -= 2 * forward;
            }
            D += 2 * sideways;
        }

        Ok(py.None())
    }
});

impl ChunkPosition {
    // Get the local_x, local_y, local_z and rotation
    // relative to a neighbor of the current chunk.
    fn relative_position(&self, py: Python, coordinate: &Coordinate, here_to_there: usize) -> (i8, i8, i8, usize) {
        // First move to the other chunk, ignoring rotations.
        let mut x = self.local_x(py).get() - CHUNK_SIZE * DISPLACEMENTS_X[here_to_there];
        let mut y = self.local_y(py).get() - CHUNK_SIZE * DISPLACEMENTS_Y[here_to_there];

        // Then rotate to match the angle between the chunks.
        let there_to_here = neighbor_index(
            coordinate,
            self.coordinate(py).borrow().deref()).unwrap();
        let rotation = (6 + there_to_here - here_to_there) % 4;
        for _ in 0..rotation {
            let temp_x = CHUNK_SIZE - y - 1;
            y = x;
            x = temp_x;
        }

        (x, y, self.local_z(py).get(), (self.rotation(py).get() + rotation) % 4)
    }

    // Cross a chunk boundary to a neighbor of this chunk,
    // also updating our local coordinates.
    // The given chunk must be a neighbor of this chunk.
    // (Otherwise, which homotopy class of paths did we use to get there?)
    fn cross_boundary(&self, py: Python, new_coordinate: Coordinate, here_to_there: usize) {
        let (x, y, z, rot) = self.relative_position(py, &new_coordinate, here_to_there);
        self.local_x(py).set(x);
        self.local_y(py).set(y);
        self.local_z(py).set(z);
        self.rotation(py).set(rot);
        let mut coord_ref = self.coordinate(py).borrow_mut();
        *coord_ref = new_coordinate;
    }
}
