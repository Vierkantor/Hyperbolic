pub extern crate piston_window;
extern crate find_folder;

use self::piston_window::{Glyphs, Input, PistonWindow, Transformed,
    clear, text,
};
use cpython::{Python, PyModule, PyObject, PyResult};
use std::cell::{RefCell};

pub fn initialize_module(py: Python, m: &PyModule) -> PyResult<()> {
    m.add(py, "Color", py.get_type::<Color>())?;
    m.add(py, "DrawQueue", py.get_type::<DrawQueue>())?;
    m.add(py, "Position", py.get_type::<Position>())?;
    Ok(())
}

py_class!(class Color |py| {
    data r : f32;
    data g : f32;
    data b : f32;
    data a : f32;

    def __new__(_cls, r: f32, g: f32, b: f32, a: f32) -> PyResult<Color> {
        Color::create_instance(py, r, g, b, a)
    }
});
impl Color {
    pub fn to_array(&self, py : Python) -> [f32; 4] {
        [*self.r(py), *self.g(py), *self.b(py), *self.a(py)]
    }
}

py_class!(class Position |py| {
    data x : f64;
    data y : f64;
    data w : f64;
    data h : f64;

    def __new__(_cls, x: f64, y: f64, w: f64, h: f64) -> PyResult<Position> {
        Position::create_instance(py, x, y, w, h)
    }
});
impl Position {
    pub fn to_array(&self, py : Python) -> [f64; 4] {
        [*self.x(py), *self.y(py), *self.w(py), *self.h(py)]
    }
}

// A basic object that can be drawn on the screen.
// Use DrawQueue::rectangle and similar for actually constructing these.
enum Drawable {
    Rectangle(Color, Position),
    Ellipse(Color, Position),
    Text(String, Color, f64, f64, u32),
}

// Make DrawQueue a public class.
// The py_class macro doesn't support public structs, apparently.
py_class!(class DrawQueue |py| {
    // The things to draw.
    // We put them in a RefCell since there might be multiple references
    // so Rust wants them to be immutable, but it's okay since it's
    // a giant bunch of side-effects anyway.
    data to_draw : RefCell<Vec<Drawable>>;

    def __new__(_cls) -> PyResult<DrawQueue> {
        DrawQueue::create_instance(py, RefCell::new(Vec::new()))
    }

    def rectangle(&self, color: Color, pos: Position) -> PyResult<PyObject> {
        self.add_drawable(py, Drawable::Rectangle(color, pos))
    }

    def ellipse(&self, color: Color, pos: Position) -> PyResult<PyObject> {
        self.add_drawable(py, Drawable::Ellipse(color, pos))
    }

    def text(&self, text: String, color: Color, x: f64, y: f64, h: u32) -> PyResult<PyObject> {
        self.add_drawable(py, Drawable::Text(text, color, x, y, h))
    }
});

impl DrawQueue {
    fn add_drawable(&self, py: Python, drawable: Drawable) -> PyResult<PyObject> {
        let mut to_draws = self.to_draw(py).borrow_mut();
        to_draws.push(drawable);
        Ok(py.None())
    }
}


// Contains the window from piston so we can convert it to an opaque Python object.
pub struct Graphics {
    pub window: PistonWindow,
    pub glyphs: Glyphs,
}

impl Graphics {
    pub fn next(&mut self) -> Option<Input> {
        self.window.next()
    }

    // Draw a list of drawables on the screen.
    fn draw<'a, T>(&mut self, py : Python, drawables : T, e : Input)
        where T : Iterator<Item = &'a Drawable>
    {
        // Move the glyph borrowing outside of the closure.
        let glyphs = &mut self.glyphs;

        self.window.draw_2d(&e, |c, g| {
            clear([1.0; 4], g);

            for drawable in drawables {
                match *drawable {
                    Drawable::Rectangle(ref color, ref pos) =>
                        piston_window::rectangle(color.to_array(py), pos.to_array(py), c.transform, g),
                    Drawable::Ellipse(ref color, ref pos) =>
                        piston_window::ellipse(color.to_array(py), pos.to_array(py), c.transform, g),
                    Drawable::Text(ref text, ref color, ref x, ref y, ref h) => {
                        let transform = c.transform.trans(*x, *y);
                        text::Text::new_color(color.to_array(py), *h).draw(
                            text,
                            glyphs,
                            &c.draw_state,
                            transform, g,
                        );
                    }
                }
            }
        });
    }

    // Get stuff to draw from Python and draw it on the screen.
    pub fn draw_py(&mut self, py: Python, drawables: PyObject, e: Input) -> PyResult<()> {
        let queue: DrawQueue = drawables.extract(py)?;
        self.draw(py, queue.to_draw(py).borrow().iter(), e);
        Ok(())
    }
}

// Start the simulation by opening a window to do graphics with.
pub fn start() -> Graphics {
    let window: piston_window::PistonWindow =
        piston_window::WindowSettings::new("Hello Piston!", [640, 480])
        .exit_on_esc(true).build().unwrap();

    let assets = find_folder::Search::ParentsThenKids(3, 3)
        .for_folder("assets").expect("could not find folder 'assets'");
    let ref font = assets.join("DejaVuSans.ttf");
    let factory = window.factory.clone();
    let glyphs = piston_window::Glyphs::new(font, factory).unwrap();

    Graphics { window: window, glyphs: glyphs }
}
