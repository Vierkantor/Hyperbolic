use cpython::{Python, PyDict, PyModule, PyResult};
use graphics::piston_window::{Key};

pub fn initialize_module(py: Python, m: &PyModule) -> PyResult<()> {
    m.add(py, "make_keyboard_table", py_fn!(py, make_keyboard_table()))?;
    Ok(())
}

fn make_keyboard_table(py: Python) -> PyResult<PyDict> {
    let result = PyDict::new(py);

    result.set_item(py, "unknown", Key::Unknown.code())?;
    result.set_item(py, "backspace", Key::Backspace.code())?;
    result.set_item(py, "tab", Key::Tab.code())?;
    result.set_item(py, "return", Key::Return.code())?;
    result.set_item(py, "escape", Key::Escape.code())?;
    result.set_item(py, "space", Key::Space.code())?;
    result.set_item(py, "exclaim", Key::Exclaim.code())?;
    result.set_item(py, "quotedbl", Key::Quotedbl.code())?;
    result.set_item(py, "hash", Key::Hash.code())?;
    result.set_item(py, "dollar", Key::Dollar.code())?;
    result.set_item(py, "percent", Key::Percent.code())?;
    result.set_item(py, "ampersand", Key::Ampersand.code())?;
    result.set_item(py, "quote", Key::Quote.code())?;
    result.set_item(py, "left_paren", Key::LeftParen.code())?;
    result.set_item(py, "right_paren", Key::RightParen.code())?;
    result.set_item(py, "asterisk", Key::Asterisk.code())?;
    result.set_item(py, "plus", Key::Plus.code())?;
    result.set_item(py, "comma", Key::Comma.code())?;
    result.set_item(py, "minus", Key::Minus.code())?;
    result.set_item(py, "period", Key::Period.code())?;
    result.set_item(py, "slash", Key::Slash.code())?;
    result.set_item(py, "d0", Key::D0.code())?;
    result.set_item(py, "d1", Key::D1.code())?;
    result.set_item(py, "d2", Key::D2.code())?;
    result.set_item(py, "d3", Key::D3.code())?;
    result.set_item(py, "d4", Key::D4.code())?;
    result.set_item(py, "d5", Key::D5.code())?;
    result.set_item(py, "d6", Key::D6.code())?;
    result.set_item(py, "d7", Key::D7.code())?;
    result.set_item(py, "d8", Key::D8.code())?;
    result.set_item(py, "d9", Key::D9.code())?;
    result.set_item(py, "colon", Key::Colon.code())?;
    result.set_item(py, "semicolon", Key::Semicolon.code())?;
    result.set_item(py, "less", Key::Less.code())?;
    result.set_item(py, "equals", Key::Equals.code())?;
    result.set_item(py, "greater", Key::Greater.code())?;
    result.set_item(py, "question", Key::Question.code())?;
    result.set_item(py, "at", Key::At.code())?;
    result.set_item(py, "left_bracket", Key::LeftBracket.code())?;
    result.set_item(py, "backslash", Key::Backslash.code())?;
    result.set_item(py, "right_bracket", Key::RightBracket.code())?;
    result.set_item(py, "caret", Key::Caret.code())?;
    result.set_item(py, "underscore", Key::Underscore.code())?;
    result.set_item(py, "backquote", Key::Backquote.code())?;
    result.set_item(py, "a", Key::A.code())?;
    result.set_item(py, "b", Key::B.code())?;
    result.set_item(py, "c", Key::C.code())?;
    result.set_item(py, "d", Key::D.code())?;
    result.set_item(py, "e", Key::E.code())?;
    result.set_item(py, "f", Key::F.code())?;
    result.set_item(py, "g", Key::G.code())?;
    result.set_item(py, "h", Key::H.code())?;
    result.set_item(py, "i", Key::I.code())?;
    result.set_item(py, "j", Key::J.code())?;
    result.set_item(py, "k", Key::K.code())?;
    result.set_item(py, "l", Key::L.code())?;
    result.set_item(py, "m", Key::M.code())?;
    result.set_item(py, "n", Key::N.code())?;
    result.set_item(py, "o", Key::O.code())?;
    result.set_item(py, "p", Key::P.code())?;
    result.set_item(py, "q", Key::Q.code())?;
    result.set_item(py, "r", Key::R.code())?;
    result.set_item(py, "s", Key::S.code())?;
    result.set_item(py, "t", Key::T.code())?;
    result.set_item(py, "u", Key::U.code())?;
    result.set_item(py, "v", Key::V.code())?;
    result.set_item(py, "w", Key::W.code())?;
    result.set_item(py, "x", Key::X.code())?;
    result.set_item(py, "y", Key::Y.code())?;
    result.set_item(py, "z", Key::Z.code())?;
    result.set_item(py, "delete", Key::Delete.code())?;
    result.set_item(py, "caps_lock", Key::CapsLock.code())?;
    result.set_item(py, "f1", Key::F1.code())?;
    result.set_item(py, "f2", Key::F2.code())?;
    result.set_item(py, "f3", Key::F3.code())?;
    result.set_item(py, "f4", Key::F4.code())?;
    result.set_item(py, "f5", Key::F5.code())?;
    result.set_item(py, "f6", Key::F6.code())?;
    result.set_item(py, "f7", Key::F7.code())?;
    result.set_item(py, "f8", Key::F8.code())?;
    result.set_item(py, "f9", Key::F9.code())?;
    result.set_item(py, "f10", Key::F10.code())?;
    result.set_item(py, "f11", Key::F11.code())?;
    result.set_item(py, "f12", Key::F12.code())?;
    result.set_item(py, "print_screen", Key::PrintScreen.code())?;
    result.set_item(py, "scroll_lock", Key::ScrollLock.code())?;
    result.set_item(py, "pause", Key::Pause.code())?;
    result.set_item(py, "insert", Key::Insert.code())?;
    result.set_item(py, "home", Key::Home.code())?;
    result.set_item(py, "page_up", Key::PageUp.code())?;
    result.set_item(py, "end", Key::End.code())?;
    result.set_item(py, "page_down", Key::PageDown.code())?;
    result.set_item(py, "right", Key::Right.code())?;
    result.set_item(py, "left", Key::Left.code())?;
    result.set_item(py, "down", Key::Down.code())?;
    result.set_item(py, "up", Key::Up.code())?;
    result.set_item(py, "num_lock_clear", Key::NumLockClear.code())?;
    result.set_item(py, "num_pad_divide", Key::NumPadDivide.code())?;
    result.set_item(py, "num_pad_multiply", Key::NumPadMultiply.code())?;
    result.set_item(py, "num_pad_minus", Key::NumPadMinus.code())?;
    result.set_item(py, "num_pad_plus", Key::NumPadPlus.code())?;
    result.set_item(py, "num_pad_enter", Key::NumPadEnter.code())?;
    result.set_item(py, "num_pad1", Key::NumPad1.code())?;
    result.set_item(py, "num_pad2", Key::NumPad2.code())?;
    result.set_item(py, "num_pad3", Key::NumPad3.code())?;
    result.set_item(py, "num_pad4", Key::NumPad4.code())?;
    result.set_item(py, "num_pad5", Key::NumPad5.code())?;
    result.set_item(py, "num_pad6", Key::NumPad6.code())?;
    result.set_item(py, "num_pad7", Key::NumPad7.code())?;
    result.set_item(py, "num_pad8", Key::NumPad8.code())?;
    result.set_item(py, "num_pad9", Key::NumPad9.code())?;
    result.set_item(py, "num_pad0", Key::NumPad0.code())?;
    result.set_item(py, "num_pad_period", Key::NumPadPeriod.code())?;
    result.set_item(py, "application", Key::Application.code())?;
    result.set_item(py, "power", Key::Power.code())?;
    result.set_item(py, "num_pad_equals", Key::NumPadEquals.code())?;
    result.set_item(py, "f13", Key::F13.code())?;
    result.set_item(py, "f14", Key::F14.code())?;
    result.set_item(py, "f15", Key::F15.code())?;
    result.set_item(py, "f16", Key::F16.code())?;
    result.set_item(py, "f17", Key::F17.code())?;
    result.set_item(py, "f18", Key::F18.code())?;
    result.set_item(py, "f19", Key::F19.code())?;
    result.set_item(py, "f20", Key::F20.code())?;
    result.set_item(py, "f21", Key::F21.code())?;
    result.set_item(py, "f22", Key::F22.code())?;
    result.set_item(py, "f23", Key::F23.code())?;
    result.set_item(py, "f24", Key::F24.code())?;
    result.set_item(py, "execute", Key::Execute.code())?;
    result.set_item(py, "help", Key::Help.code())?;
    result.set_item(py, "menu", Key::Menu.code())?;
    result.set_item(py, "select", Key::Select.code())?;
    result.set_item(py, "stop", Key::Stop.code())?;
    result.set_item(py, "again", Key::Again.code())?;
    result.set_item(py, "undo", Key::Undo.code())?;
    result.set_item(py, "cut", Key::Cut.code())?;
    result.set_item(py, "copy", Key::Copy.code())?;
    result.set_item(py, "paste", Key::Paste.code())?;
    result.set_item(py, "find", Key::Find.code())?;
    result.set_item(py, "mute", Key::Mute.code())?;
    result.set_item(py, "volume_up", Key::VolumeUp.code())?;
    result.set_item(py, "volume_down", Key::VolumeDown.code())?;
    result.set_item(py, "num_pad_comma", Key::NumPadComma.code())?;
    result.set_item(py, "num_pad_equals_as400", Key::NumPadEqualsAS400.code())?;
    result.set_item(py, "alt_erase", Key::AltErase.code())?;
    result.set_item(py, "sysreq", Key::Sysreq.code())?;
    result.set_item(py, "cancel", Key::Cancel.code())?;
    result.set_item(py, "clear", Key::Clear.code())?;
    result.set_item(py, "prior", Key::Prior.code())?;
    result.set_item(py, "return2", Key::Return2.code())?;
    result.set_item(py, "separator", Key::Separator.code())?;
    result.set_item(py, "out", Key::Out.code())?;
    result.set_item(py, "oper", Key::Oper.code())?;
    result.set_item(py, "clear_again", Key::ClearAgain.code())?;
    result.set_item(py, "cr_sel", Key::CrSel.code())?;
    result.set_item(py, "ex_sel", Key::ExSel.code())?;
    result.set_item(py, "num_pad00", Key::NumPad00.code())?;
    result.set_item(py, "num_pad000", Key::NumPad000.code())?;
    result.set_item(py, "thousands_separator", Key::ThousandsSeparator.code())?;
    result.set_item(py, "decimal_separator", Key::DecimalSeparator.code())?;
    result.set_item(py, "currency_unit", Key::CurrencyUnit.code())?;
    result.set_item(py, "currency_sub_unit", Key::CurrencySubUnit.code())?;
    result.set_item(py, "num_pad_left_paren", Key::NumPadLeftParen.code())?;
    result.set_item(py, "num_pad_right_paren", Key::NumPadRightParen.code())?;
    result.set_item(py, "num_pad_left_brace", Key::NumPadLeftBrace.code())?;
    result.set_item(py, "num_pad_right_brace", Key::NumPadRightBrace.code())?;
    result.set_item(py, "num_pad_tab", Key::NumPadTab.code())?;
    result.set_item(py, "num_pad_backspace", Key::NumPadBackspace.code())?;
    result.set_item(py, "num_pad_a", Key::NumPadA.code())?;
    result.set_item(py, "num_pad_b", Key::NumPadB.code())?;
    result.set_item(py, "num_pad_c", Key::NumPadC.code())?;
    result.set_item(py, "num_pad_d", Key::NumPadD.code())?;
    result.set_item(py, "num_pad_e", Key::NumPadE.code())?;
    result.set_item(py, "num_pad_f", Key::NumPadF.code())?;
    result.set_item(py, "num_pad_xor", Key::NumPadXor.code())?;
    result.set_item(py, "num_pad_power", Key::NumPadPower.code())?;
    result.set_item(py, "num_pad_percent", Key::NumPadPercent.code())?;
    result.set_item(py, "num_pad_less", Key::NumPadLess.code())?;
    result.set_item(py, "num_pad_greater", Key::NumPadGreater.code())?;
    result.set_item(py, "num_pad_ampersand", Key::NumPadAmpersand.code())?;
    result.set_item(py, "num_pad_dbl_ampersand", Key::NumPadDblAmpersand.code())?;
    result.set_item(py, "num_pad_vertical_bar", Key::NumPadVerticalBar.code())?;
    result.set_item(py, "num_pad_dbl_vertical_bar", Key::NumPadDblVerticalBar.code())?;
    result.set_item(py, "num_pad_colon", Key::NumPadColon.code())?;
    result.set_item(py, "num_pad_hash", Key::NumPadHash.code())?;
    result.set_item(py, "num_pad_space", Key::NumPadSpace.code())?;
    result.set_item(py, "num_pad_at", Key::NumPadAt.code())?;
    result.set_item(py, "num_pad_exclam", Key::NumPadExclam.code())?;
    result.set_item(py, "num_pad_mem_store", Key::NumPadMemStore.code())?;
    result.set_item(py, "num_pad_mem_recall", Key::NumPadMemRecall.code())?;
    result.set_item(py, "num_pad_mem_clear", Key::NumPadMemClear.code())?;
    result.set_item(py, "num_pad_mem_add", Key::NumPadMemAdd.code())?;
    result.set_item(py, "num_pad_mem_subtract", Key::NumPadMemSubtract.code())?;
    result.set_item(py, "num_pad_mem_multiply", Key::NumPadMemMultiply.code())?;
    result.set_item(py, "num_pad_mem_divide", Key::NumPadMemDivide.code())?;
    result.set_item(py, "num_pad_plus_minus", Key::NumPadPlusMinus.code())?;
    result.set_item(py, "num_pad_clear", Key::NumPadClear.code())?;
    result.set_item(py, "num_pad_clear_entry", Key::NumPadClearEntry.code())?;
    result.set_item(py, "num_pad_binary", Key::NumPadBinary.code())?;
    result.set_item(py, "num_pad_octal", Key::NumPadOctal.code())?;
    result.set_item(py, "num_pad_decimal", Key::NumPadDecimal.code())?;
    result.set_item(py, "num_pad_hexadecimal", Key::NumPadHexadecimal.code())?;
    result.set_item(py, "l_ctrl", Key::LCtrl.code())?;
    result.set_item(py, "l_shift", Key::LShift.code())?;
    result.set_item(py, "l_alt", Key::LAlt.code())?;
    result.set_item(py, "l_gui", Key::LGui.code())?;
    result.set_item(py, "r_ctrl", Key::RCtrl.code())?;
    result.set_item(py, "r_shift", Key::RShift.code())?;
    result.set_item(py, "r_alt", Key::RAlt.code())?;
    result.set_item(py, "r_gui", Key::RGui.code())?;
    result.set_item(py, "mode", Key::Mode.code())?;
    result.set_item(py, "audio_next", Key::AudioNext.code())?;
    result.set_item(py, "audio_prev", Key::AudioPrev.code())?;
    result.set_item(py, "audio_stop", Key::AudioStop.code())?;
    result.set_item(py, "audio_play", Key::AudioPlay.code())?;
    result.set_item(py, "audio_mute", Key::AudioMute.code())?;
    result.set_item(py, "media_select", Key::MediaSelect.code())?;
    result.set_item(py, "www", Key::Www.code())?;
    result.set_item(py, "mail", Key::Mail.code())?;
    result.set_item(py, "calculator", Key::Calculator.code())?;
    result.set_item(py, "computer", Key::Computer.code())?;
    result.set_item(py, "ac_search", Key::AcSearch.code())?;
    result.set_item(py, "ac_home", Key::AcHome.code())?;
    result.set_item(py, "ac_back", Key::AcBack.code())?;
    result.set_item(py, "ac_forward", Key::AcForward.code())?;
    result.set_item(py, "ac_stop", Key::AcStop.code())?;
    result.set_item(py, "ac_refresh", Key::AcRefresh.code())?;
    result.set_item(py, "ac_bookmarks", Key::AcBookmarks.code())?;
    result.set_item(py, "brightness_down", Key::BrightnessDown.code())?;
    result.set_item(py, "brightness_up", Key::BrightnessUp.code())?;
    result.set_item(py, "display_switch", Key::DisplaySwitch.code())?;
    result.set_item(py, "kbd_illum_toggle", Key::KbdIllumToggle.code())?;
    result.set_item(py, "kbd_illum_down", Key::KbdIllumDown.code())?;
    result.set_item(py, "kbd_illum_up", Key::KbdIllumUp.code())?;
    result.set_item(py, "eject", Key::Eject.code())?;
    result.set_item(py, "sleep", Key::Sleep.code())?;

    Ok(result)
}
