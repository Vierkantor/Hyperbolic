#![feature(plugin)]
#![plugin(quickcheck_macros)]

#[macro_use] extern crate cpython;
#[macro_use] extern crate custom_derive;
#[macro_use] extern crate newtype_derive;

use cpython::{
    PyObject, PyResult, PyTuple, Python,
    ObjectProtocol,
    PythonObject, ToPyObject,
};
use graphics::piston_window::{Button, Input};

mod coordinate;
mod graphics;
mod input;
mod position;

py_module_initializer!(_rust, init_rust, PyInit__rust, |py, m| {
    m.add(py, "__doc__", "Rust implementations of a hyperbolic simulator.")?;
    m.add(py, "main", py_fn!(py, main(
        update: PyObject,
        draw: PyObject,
        input: PyObject
    )))?;
    coordinate::initialize_module(py, m)?;
    graphics::initialize_module(py, m)?;
    input::initialize_module(py, m)?;
    position::initialize_module(py, m)?;
    Ok(())
});

pub mod path;

#[cfg(test)]
extern crate quickcheck;
#[cfg(test)]
mod tests {
    use path;

    #[test]
    fn empty_path_to_empty_path() {
        let empty_abs_path = path::AbsPath::new();
        assert_eq!(path::abs_to_rel(&empty_abs_path), None);
        let empty_rel_path = path::RelPath::new();
        assert_eq!(path::rel_to_abs(path::AbsStep::North, &empty_rel_path), empty_abs_path);
    }

    #[quickcheck]
    fn add_difference(dir1: path::AbsStep, dir2: path::AbsStep) {
        assert_eq!(path::rel_sum(dir1, path::abs_difference(dir1, dir2)), dir2)
    }

    #[quickcheck]
    fn rel_to_abs_to_rel(direction: path::AbsStep, mut rel_path: path::RelPath) {
        // Make sure we have a starting step, so conversion makes sense.
        rel_path.insert(0, path::RelStep::Forward);
        let abs_path = path::rel_to_abs(direction, &rel_path);
        let rel_path2 = path::abs_to_rel(&abs_path);
        assert_eq!(Some((direction, rel_path)), rel_path2)
    }
}

fn main(py: Python, update: PyObject, draw: PyObject, input: PyObject) -> PyResult<PyObject> {
    let mut window: graphics::Graphics = graphics::start();
    while let Some(e) = window.next() {
        match e {
            Input::Update(u) => {
                let args = PyTuple::new(py, &[
                    u.dt.into_py_object(py).into_object(),
                ]);
                let _ = update.call(py, args, None)?;
            }
            Input::Render(r) => {
                let args = PyTuple::new(py, &[
                    r.draw_width.into_py_object(py).into_object(),
                    r.draw_height.into_py_object(py).into_object(),
                ]);
                let draw_result = draw.call(py, args, None)?;
                window.draw_py(py, draw_result, e)?;
            }
            Input::Press(Button::Keyboard(key)) => {
                let args = PyTuple::new(py, &[
                    true.into_py_object(py).into_object(),
                    key.code().into_py_object(py).into_object(),
                ]);
                let _ = input.call(py, args, None)?;
            }
            Input::Release(Button::Keyboard(key)) => {
                let args = PyTuple::new(py, &[
                    false.into_py_object(py).into_object(),
                    key.code().into_py_object(py).into_object(),
                ]);
                let _ = input.call(py, args, None)?;
            }
            // We will process keyboard input ourselves.
            Input::Text(_t) => { }

            // TODO: do something useful here?
            Input::Resize(_w, _h) => { }
            Input::Focus(_f) => { }
            Input::Close(_c) => { }
            Input::AfterRender(_a) => { }
            Input::Idle(_i) => { }
            Input::Custom(_id, _arg) => { }

            // Ignore controller events.
            Input::Press(Button::Controller(_cb)) => { }
            Input::Release(Button::Controller(_cb)) => { }

            // Ignore mouse events.
            Input::Press(Button::Mouse(_mb)) => { }
            Input::Release(Button::Mouse(_mb)) => { }
            Input::Move(_m) => { }
            Input::Cursor(_c) => { }
        }
    }
    Ok(py.None())
}
