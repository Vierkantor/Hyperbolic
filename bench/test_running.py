"""Test running the simulation interactively,
even if we have no interactivity."""

from hyperbolic.running import initialize, draw

def test_draw_benchmark(benchmark):
	"""Test the speed of finding everything to draw."""
	initialize([])

	benchmark(draw, 1024, 768)
