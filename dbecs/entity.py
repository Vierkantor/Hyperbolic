"""DBECS: the DataBase Entity Component System.

This is an entity-component system which allows an entity and its components
to be easily written to a database, e.g. when unloading
or when saving. Similarly, loading a component is also quite easy.

We use SQLAlchemy as the backend for the database,
but this should not be anything to worry about when you use the system.
"""

import sqlalchemy

class NoComponent(Exception):
	"""Raised when an entity does not have a component,
	but is treated like it does have.
	"""
	def __init__(self, component, entity):
		super().__init__("{} is not {}".format(entity, component))
class HasComponent(Exception):
	"""Raised when an entity already has a component,
	but is treated like it doesn't have one.
	"""
	def __init__(self, component, entity):
		super().__init__("{} is already {}".format(entity, component))

"""The components that have been defined up to now,
so they can get stored automatically.
"""
known_components = {}

def new_save(engine):
	"""Initialize a new save file using the given engine.

	When making a save file, this uses the list of known components.
	to make sure all tables are available to be stored.
	This requires that you don't programmatically create new Components
	after the save game has been started.

	This does not allow you to update save file structures!
	Use a tool like alembic to import save files
	from your older editions.

	The return value contains the table data needed for saving.
	"""

	metadata = sqlalchemy.MetaData()
	entity_table, component_tables = generate_tables(metadata)
	metadata.create_all(engine)

	return (entity_table, component_tables)

def generate_tables(metadata):
	"""Generate the tables for the system with all known components.

	Returns a tuple containing a table for entities,
	tupled with a map of component classes to tables.
	These tables will be used to store attributes when saving.
	"""

	tables = {}
	for cls in known_components.values():
		tables[cls] = cls.generate_table(metadata)
	return (Entity.generate_table(metadata), tables)

"""Represent Python types as SQLAlchemy types."""
value_to_sql = {
		int: sqlalchemy.Integer,
		str: sqlalchemy.String,
}
def add_value_type(value_type, sql_type):
	"""Add a new option for Values to be represented in SQL.

	It's usually a better idea to directly add it to the value_to_sql dict,
	except when you need some form of circularity.
	"""
	value_to_sql[value_type] = sql_type

class Value:
	"""Represents a single attribute owned by a Component."""
	def __init__(self, type):
		self.type = type
		self.sql_type = value_to_sql[type]

	def __get__(self, inst, cls):
		if inst is None:
			return self
		else:
			try:
				return inst.attrs[self.name]
			except KeyError:
				raise AttributeError(self.name)

	def __set__(self, inst, value):
		if not isinstance(value, self.type):
			raise TypeError("wrong type {} for {}".format(type(value), self.name))
		else:
			inst.attrs[self.name] = value

	def __set_name__(self, cls, name):
		self.name = name

class MetaComponent(type):
	"""The metaclass that Components use to store themselves in a database.

	Basically registers all Value class attributes for later use.
	"""
	def __init__(self, name, bases, attrs):
		super().__init__(name, bases, attrs)
		known_components[name] = self

	def __new__(metacls, cls, bases, clsdict):
		"""Construct a new class stored in the database."""
		fields = {}
		for name, attr in clsdict.items():
			if isinstance(attr, Value):
				fields[name] = attr
		# copy over the base class's fields
		# TODO: throw exception in the case of duplicate fields
		for base in bases:
			try:
				base_fields = base._fields
			except AttributeError:
				continue
			fields.update(base_fields)
		clsdict["_fields"] = fields

		# Ensure no other attributes will get assigned
		clsdict["__slots__"] = ["attrs", "_entity"]

		return super().__new__(metacls, cls, bases, clsdict)

class Component(metaclass=MetaComponent):
	"""An abstract base class for components of the entity-component system.
	
	When modelling a full world, you want its parts to interact
	without really having access to the full details every time.
	For example, when you specify that you can lock a door or a box,
	you don't want the key to know all about the room connection mechanics
	and about inventory access mechanics.
	You could also accomplish this using regular old compositionality,
	but this should be a nicer explicit way.

	Attributes belonging to the component must be declared as class attributes,
	and may only have one type, to avoid issues when storing them in the database.

	Components don't have "real" instances. Instead, whenever you write:
		> component = Lockable(door)
	you will get a wrapper through which you can interact, e.g.:
		> component.lockWith(key)
	Interacting with this wrapper will change the relevant attributes on the
	original entity. If later on you call this code again:
		> component2 = Lockable(door)
	then the attributes will remain changed:
		> assert component2.locked == True
	Components are smart enough to avoid namespace collisions,
	but do not yet support attribute inheritance.
	"""

	def __init__(self, entity):
		"""Interact with an entity through this component."""
		# unwrap the previous component from the entity
		if isinstance(entity, Component):
			entity = entity._entity

		cls = type(self)
		self._entity = entity
		try:
			self.attrs = self._entity.components[cls]
		except KeyError:
			raise NoComponent(cls, entity)

	@classmethod
	def register(cls, entity):
		"""Called when this component is initialized on the entity.
		
		You can e.g. use it to set default values for attributes,
		or install another required component.

		See also Component.load.
		"""
		pass

	@classmethod
	def load(cls, entity, attr_list):
		"""Apply this component from a list of attributes.

		This method is called after Component.register.
		"""
		for key, value in attr_list:
			entity.components[cls][key] = value

	@classmethod
	def generate_table(cls, metadata):
		"""Create a new SQLAlchemy table which can store this component's attributes.
		"""
		rows = [sqlalchemy.Column('_entity', sqlalchemy.Integer)]
		for name, attr in cls._fields.items():
			rows.append(sqlalchemy.Column(name, attr.sql_type))
		return sqlalchemy.Table(cls.__name__, metadata, *rows)

	@classmethod
	def save(cls, attrs):
		"""Convert the instance attributes to values suitable for SQL.

		Both attributes and values are a dictionary mapping string keys.

		Usually you want to use the default implementation,
		except when you have some optimized in-memory representation
		that you need to serialize in some way.
		"""
		return attrs

class Entity:
	"""A base class for entities of the entity-component system.

	See Component for more details on how to use this.
	"""

	def __init__(self):
		# TODO: a constructor with a list of components?
		self.components = {}

		# When this entity is in the database, it stores this id.
		# This also ensures references remain intact after reloading.
		self._id = id(self)

	def add_component(self, component, *args, **kwargs):
		"""Add a new component to this entity.
		
		Other arguments are passed to component.register.
		
		Raises an error when the entity already has the specified component.
		"""
		if component in self.components:
			raise HasComponent(component, self)
		self.components[component] = dict()
		component.register(self, *args, **kwargs)

	@classmethod
	def generate_table(cls, metadata):
		"""Give the table needed to store Entity data."""
		return sqlalchemy.Table('entity', metadata,
				sqlalchemy.Column('id', sqlalchemy.Integer, primary_key=True),
		);

	def save(self, connection, tables):
		"""Save this entity to the database.

		The tables argument is the result of new_save() and/or
		generate_tables(MetaData()).
		"""
		entity_table, component_tables = tables

		# insert this entity into the file
		connection.execute(entity_table.insert(), id=self._id)

		# insert each component
		for component, attrs in self.components.items():
			insertion = component_tables[component].insert()
			values = component.save(attrs)
			connection.execute(insertion, _entity=self._id, **values)

	@classmethod
	def load_all(cls, connection, tables):
		"""Get all entities stored in the database.
		
		Returns a dictionary entity_id -> entity.
		"""
		entity_table, component_tables = tables

		# load entity data
		query = sqlalchemy.select([entity_table])
		result = connection.execute(query)
		entities = {}
		for entity_id, in result:
			entities[entity_id] = cls()
			entities[entity_id]._id = entity_id
		result.close()

		# load component data
		for component_cls, table in component_tables.items():
			query = sqlalchemy.select([table])
			result = connection.execute(query)
			for row in result:
				entity = entities[row._entity]
				entity.add_component(component_cls)
				component_cls.load(entity, row.items())
			result.close()

		return entities
