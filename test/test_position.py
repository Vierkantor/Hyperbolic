"""Tests for positions within a chunk."""

import hypothesis
import hypothesis.strategies as st
import pytest
import strategies as st

import sqlalchemy

from dbecs.entity import Entity, new_save
from hyperbolic.chunk import _chunk_size
from hyperbolic.coordinate import origin, neighbors, neighbors_from
from hyperbolic.position import Positioned, rotations, north, east, south, west

def test_rotation_order():
	"""The rotations should be in clockwise order."""
	assert rotations == (north, east, south, west)

def test_rotate_right():
	"""As you rotate right, you should turn clockwise
	until after 4 rotations you end up at the start.
	"""
	entity = Entity()
	entity.add_component(Positioned, origin, 0, 0, 0, rotations[0])
	pos = Positioned(entity)
	for rotation in rotations:
		assert pos.rotation == rotation
		pos.turn_right()
	assert pos.rotation == rotations[0]

def test_rotate_left():
	"""As you rotate left, you should turn counterclockwise
	until after 4 rotations you end up at the start.
	"""
	entity = Entity()
	entity.add_component(Positioned, origin, 0, 0, 0, rotations[0])
	pos = Positioned(entity)
	for rotation in rotations[::-1]:
		pos.turn_left()
		assert pos.rotation == rotation

def test_forward_from_origin():
	"""Going 1 step forward from ;0,0,0;0 puts you at y;15,15,0;3."""
	# COMPAT: uses kbword-based coordinates.
	entity = Entity()
	entity.add_component(Positioned, '', 0, 0, 0, rotations[0])
	pos = Positioned(entity)
	pos.go_forward(1)

	assert pos.coordinate == 'y'
	assert pos.rotation == 3
	assert pos.local_x == 15
	assert pos.local_y == 15
	assert pos.local_z == 0

def test_cycle_five_times():
	"""Going forward a chunk and to the right five times
	should put you back in the origin.
	"""
	entity = Entity()
	entity.add_component(Positioned, origin, 0, 0, 0, rotations[0])
	pos = Positioned(entity)
	next = neighbors(pos.coordinate)[pos.rotation]
	for _cycle in range(0, 5):
		prev = pos.coordinate
		pos.go_forward(_chunk_size)
		pos.turn_right()
		assert pos.coordinate == next

		next = neighbors_from(pos.coordinate, prev)[3]
	assert pos.coordinate == origin
	assert pos.rotation == rotations[0]

def test_short_cycle_five_times():
	"""We start at the top left border of a chunk,
	so going forward one square and to the left five times should also be a loop.
	"""
	entity = Entity()
	entity.add_component(Positioned, origin, 0, 0, 0, rotations[0])
	pos = Positioned(entity)
	next = neighbors(pos.coordinate)[pos.rotation]
	for _cycle in range(0, 5):
		prev = pos.coordinate
		pos.go_forward()
		pos.turn_left()

		assert pos.coordinate == next

		next = neighbors_from(pos.coordinate, prev)[1]
	assert pos.coordinate == origin
	assert pos.rotation == rotations[0]
	assert pos.local_x == pos.local_y == pos.local_z == 0

def test_cycle_both_ways():
	"""Going around a corner in both directions,
	we should encounter tiles in opposite order.
	"""
	entity1 = Entity()
	entity1.add_component(Positioned, origin, 0, 0, 0, rotations[0])
	pos1 = Positioned(entity1)
	encountered1 = ['']
	for _cycle in range(0, 5):
		pos1.go_forward()
		pos1.turn_left()
		encountered1.append(pos1.coordinate)

	entity2 = Entity()
	entity2.add_component(Positioned, origin, 0, 0, 0, rotations[3])
	pos2 = Positioned(entity2)
	encountered2 = ['']
	for _cycle in range(0, 5):
		pos2.go_forward()
		pos2.turn_right()
		encountered2.append(pos2.coordinate)

	assert encountered1 == encountered2[::-1]

@hypothesis.given(st.coordinates, st.local_coord)
def test_move_forward(coordinate, distance):
	"""When you move in the forward direction,
	you get the same as going forward."""
	
	entity = Entity()
	entity.add_component(Positioned, coordinate, 0, 0, 0, rotations[0])
	pos = Positioned(entity)
	pos.go_forward(distance)

	entity2 = Entity()
	entity2.add_component(Positioned, coordinate, 0, 0, 0, rotations[0])
	pos2 = Positioned(entity2)
	pos2.move(distance, 0, 0)

	assert pos.coordinate == pos2.coordinate
	assert pos.local_x == pos2.local_x
	assert pos.local_y == pos2.local_y
	assert pos.local_z == pos2.local_z
	assert pos.rotation == pos2.rotation

@hypothesis.given(st.coordinates, st.local_coord)
def test_move_backward(coordinate, distance):
	"""When you move in the backward direction,
	you get the same as going backward orthogonally."""
	
	entity = Entity()
	entity.add_component(Positioned, coordinate, 0, 0, 0, rotations[0])
	pos = Positioned(entity)
	pos.go_orthogonal(rotations[2], distance)

	entity2 = Entity()
	entity2.add_component(Positioned, coordinate, 0, 0, 0, rotations[0])
	pos2 = Positioned(entity2)
	pos2.move(-distance, 0, 0)

	assert pos.coordinate == pos2.coordinate
	assert pos.local_x == pos2.local_x
	assert pos.local_y == pos2.local_y
	assert pos.local_z == pos2.local_z
	assert pos.rotation == pos2.rotation

@hypothesis.given(st.coordinates, st.local_coords, st.local_coords)
def test_move_within_chunk(coordinate, local_coords, displacement):
	"""Moving within a chunk is the same as adding coordinates."""

	x, y, z = local_coords
	dx, dy, dz = displacement
	hypothesis.assume(0 <= x + dx < _chunk_size)
	hypothesis.assume(0 <= y + dy < _chunk_size)
	hypothesis.assume(0 <= z + dz < _chunk_size)

	entity = Entity()
	entity.add_component(Positioned, coordinate, x, y, z, rotations[1])
	pos = Positioned(entity)
	pos.move(dx, dy, dz)

	assert pos.coordinate == coordinate
	assert pos.local_x == x + dx
	assert pos.local_y == y + dy
	assert pos.local_z == z + dz
	assert pos.rotation == 1

@hypothesis.given(st.coordinates, st.local_coords, st.rotations)
def test_save_load(coordinate, local_coord, rotation):
	"""Create a Positioned entity, save it, and load it."""
	entity = Entity()
	entity.add_component(Positioned, coordinate, *local_coord, rotation)

	# Save the entity...
	url = "sqlite:///:memory:"
	engine = sqlalchemy.create_engine(url)
	save_tables = new_save(engine)
	connection = engine.connect()
	entity.save(connection, save_tables)

	# ...and load it again.
	entities = Entity.load_all(connection, save_tables)
	assert len(entities) == 1
	entity2 = list(entities.values())[0]
	component2 = Positioned(entity2)
	assert component2.coordinate == coordinate
	assert component2.local_x == local_coord[0]
	assert component2.local_y == local_coord[1]
	assert component2.local_z == local_coord[2]
	assert component2.rotation == rotation
