"""Test some laws that every coordinate system should adhere to.

If you have a test that is specific to an implementation,
please put it in a new file so we can swap it out more easily.
(At least, I'm pretty sure the Knuth-Bendix word system is not the final choice.)
"""

import pytest

from hyperbolic.coordinate import origin, neighbors, neighbor_index, neighbors_from

def test_origin_neighbors_antisymmetric():
	"""The origin tile should be a neighbor of its neighbors."""
	for neighbor in neighbors(origin):
		assert origin in neighbors(neighbor)

def test_origin_neighbor_index():
	"""The neighbor index of the origin tile's neighbors should match the list."""
	tile = neighbors(origin)[0]

	for i, neighbor in enumerate(neighbors(origin)):
		assert i == neighbor_index(origin, neighbor)

def test_neighbors_from_is_neighbors():
	"""The neighbors from the origin should be a rotated set of neighbors."""
	tile = neighbors(origin)[0]

	from_origin = neighbors_from(tile, origin)
	unrotated = neighbors(tile)

	for neighbor2 in from_origin:
		assert neighbor2 in unrotated
	for neighbor2 in unrotated:
		assert neighbor2 in from_origin

def test_cycle_five_times():
	"""Going forward and to the right five times should put you back in the origin."""
	step0 = origin
	step1 = neighbors(step0)[1]
	step2 = neighbors_from(step1, step0)[1]
	step3 = neighbors_from(step2, step1)[1]
	step4 = neighbors_from(step3, step2)[1]
	step5 = neighbors_from(step4, step3)[1]
	assert step0 == step5
	assert len(set([step0, step1, step2, step3, step4, step5])) == 5
