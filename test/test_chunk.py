"""Tests for getting and setting chunks."""

import pytest

from hyperbolic.chunk import get_chunk
from hyperbolic.coordinate import origin

def test_get_chunk_twice():
	"""Getting a chunk twice should give an identical object."""
	chunk1 = get_chunk(origin)
	chunk2 = get_chunk(origin)
	assert chunk1 == chunk2
