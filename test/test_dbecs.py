"""Tests for the DataBase Entity Component System."""

import pytest
import sqlalchemy

from dbecs.entity import Entity, Component, HasComponent, NoComponent, Value, new_save

def test_no_component():
	"""Receive an error when an entity does not have the required component."""
	entity = Entity()
	class ComponentHaving(Component):
		"""A dummy component."""
		pass
	with pytest.raises(NoComponent):
		ComponentHaving(entity)

def test_two_component():
	"""Receive an error when an entity already has a component."""
	entity = Entity()
	class ComponentHaving(Component):
		"""A dummy component."""
		pass
	entity.add_component(ComponentHaving)
	with pytest.raises(HasComponent):
		entity.add_component(ComponentHaving)

def test_valueless():
	"""An attribute without a corresponding Value raises a NameError."""
	entity = Entity()

	class AttributeHaving(Component):
		"""Can set some attribute on the entity."""

		def set_some_attribute(self, value):
			"""Store the value on the entity."""
			self.some_attribute = value

	entity.add_component(AttributeHaving)
	component = AttributeHaving(entity)
	with pytest.raises(AttributeError):
		component.set_some_attribute(37)

def test_set_and_get():
	"""Create a new entity, use a component to set an attribute and read it."""
	entity = Entity()

	class AttributeHaving(Component):
		"""Can set and get some attribute on the entity."""

		some_attribute = Value(int)

		def set_some_attribute(self, value):
			"""Store the value on the entity."""
			self.some_attribute = value
		def get_some_attribute(self):
			"""Read the value from the entity."""
			return self.some_attribute

	entity.add_component(AttributeHaving)

	component = AttributeHaving(entity)
	component.set_some_attribute(37)

	component2 = AttributeHaving(entity)
	assert component2.get_some_attribute() == 37

def test_new_save():
	"""Define a new component and ensure it will get added to the save file."""
	class AttributeHaving(Component):
		"""Can set and get some attribute on the entity."""

		some_attribute = Value(int)

		def set_some_attribute(self, value):
			"""Store the value on the entity."""
			self.some_attribute = value
		def get_some_attribute(self):
			"""Read the value from the entity."""
			return self.some_attribute

	url = "sqlite:///:memory:"
	entity_table, component_tables = new_save(sqlalchemy.create_engine(url))
	assert list(map(lambda col: col.name, entity_table.columns)) == ['id']
	assert list(map(lambda col: col.name, component_tables[AttributeHaving].columns)) == ['_entity', 'some_attribute']

def test_save_something():
	"""Define a new component and ensure it will get added to the save file."""
	class AttributeHaving(Component):
		"""Can set and get some attribute on the entity."""

		some_attribute = Value(int)

		def set_some_attribute(self, value):
			"""Store the value on the entity."""
			self.some_attribute = value
		def get_some_attribute(self):
			"""Read the value from the entity."""
			return self.some_attribute

	# Define an entity with some attribute...
	entity = Entity()
	entity.add_component(AttributeHaving)
	component = AttributeHaving(entity)
	component.set_some_attribute(37)

	# save the entity...
	url = "sqlite:///:memory:"
	engine = sqlalchemy.create_engine(url)
	save_tables = entity_table, component_tables = new_save(engine)
	connection = engine.connect()
	entity.save(connection, save_tables)

	query = sqlalchemy.select([entity_table])
	entity_rows = list(connection.execute(query))
	assert len(entity_rows) == 1
	assert entity_rows[0].id == entity._id
	query = sqlalchemy.select([component_tables[AttributeHaving]])
	component_rows = list(connection.execute(query))
	assert len(component_rows) == 1
	assert component_rows[0]._entity == entity._id
	assert component_rows[0].some_attribute == 37

def test_save_load():
	"""Create an entity with a component, save it and reload it."""

	class AttributeHaving(Component):
		"""Can set and get some attribute on the entity."""

		some_attribute = Value(int)

		def set_some_attribute(self, value):
			"""Store the value on the entity."""
			self.some_attribute = value
		def get_some_attribute(self):
			"""Read the value from the entity."""
			return self.some_attribute

	# Define an entity with some attribute...
	entity = Entity()
	entity.add_component(AttributeHaving)
	component = AttributeHaving(entity)
	component.set_some_attribute(37)

	# save the entity...
	url = "sqlite:///:memory:"
	engine = sqlalchemy.create_engine(url)
	save_tables = new_save(engine)
	connection = engine.connect()
	entity.save(connection, save_tables)

	# ...and load it again.
	entities = Entity.load_all(connection, save_tables)
	assert len(entities) == 1
	entity2 = list(entities.values())[0]
	component2 = AttributeHaving(entity2)
	assert component2.get_some_attribute() == 37
