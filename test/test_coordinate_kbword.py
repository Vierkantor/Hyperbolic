"""Unit tests for the Knuth-Bendix word system of coordinates.

If you have a test that is specific to that implementation of coordinates,
please put it in this file so we can swap it out more easily.
(At least, I'm pretty sure the Knuth-Bendix word system is not the final choice.)
"""

import hypothesis
import hypothesis.strategies as st
import pytest

import hyperbolic.coordinate.kbword as py
import hyperbolic._rust as rust

@hypothesis.given(st.text('xy'))
def test_rust_is_py(word):
	"""The Rust code should do the same as the Python code."""
	assert rust._shorten(word) == py._shorten(word)
	assert rust._normalize(word) == py._normalize(word)

	assert rust.neighbors(word) == py.neighbors(word)

@hypothesis.given(st.text('xy'))
def test_shorten_step_shortens(word):
	"""Ensure _shorten_step gives a shorter word according to the shortlex ordering."""
	shorter = rust._shorten_step(word)
	assert len(shorter) < len(word) or (len(shorter) == len(word) and shorter <= word)

@hypothesis.given(st.text('xy'))
def test_shorten_shortens_fully(word):
	"""Ensure the result of _shorten can't get shorter.
	
	(i.e. it's a fixed point of _shorten_step)
	"""
	shortened = rust._shorten(word)
	assert rust._shorten_step(shortened) == shortened

def test_element_orders():
	"""Shortening something that should be equal to '' should give ''."""
	assert rust._shorten('') == ''
	assert rust._shorten('x'*4) == ''
	assert rust._shorten('y'*5) == ''
	assert rust._shorten('xy'*2) == ''

@hypothesis.given(st.text('xy'))
def test_normalize_rotation(word):
	"""All rotations of a word should normalize to the same thing."""
	normalized = rust._normalize(word)
	for i in range(0, 4):
		assert rust._normalize(word + 'x'*i) == normalized

@hypothesis.given(st.text('xy'), st.text('xy'))
def test_homomorphic_shortening(word1, word2):
	"""Shortening two words can be done before and after concatenation."""
	assert rust._shorten(word1 + word2) == rust._shorten(rust._shorten(word1) + rust._shorten(word2))

def test_specific_words():
	"""Because the rewrite rules are a giant heap of words,
	work out a couple of examples manually and check they correspond.
	"""
	assert rust._normalize(py._north) == "y"
	assert rust._normalize(py._north + py._west) == "yxxy"
	assert rust._normalize(py._west + py._north) == "xxxyy"
