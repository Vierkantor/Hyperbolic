"""Useful strategies for generating Hypothesis values."""

import hypothesis
import hypothesis.strategies as st
import pytest

from hyperbolic.chunk import _chunk_size
from hyperbolic.coordinate.kbword import _normalize, _shorten, _shorten_step

# Generate a tile coordinate.
# COMPAT: uses kbwords-based coordinates.
coordinates = st.text('xy').map(_normalize)
# Generate one component of the local coordinates.
local_coord = st.integers(0, _chunk_size - 1) # Hypothesis generates inclusive ranges.
# Generate a tuple of local x, y, z coordinates.
local_coords = st.tuples(local_coord, local_coord, local_coord)
# Generate a random rotation in one of the orthogonal directions.
rotations = st.integers(0, 3)
