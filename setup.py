#!/usr/bin/env python3

from setuptools import setup
from setuptools_rust import RustExtension

setup(name='hyperbolic-rust',
		version='0.1',
		rust_extensions=[RustExtension(
			'hyperbolic._rust',
			'extensions/Cargo.toml',
		)],
		packages=['dbecs', 'hyperbolic', 'hyperbolic.coordinate'],
		zip_safe=False,
)
